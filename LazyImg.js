document.addEventListener("DOMContentLoaded", function () {
    var lazyImg = [].slice.call(document.querySelectorAll("img.lazy, .lazy-bg, .lazy-vid"));
    
    console.log(lazyImg);
    
    if ("IntersectionObserver" in window) {
        let lazyImageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    console.log("lazy img " + entry.target.classList.contains('lazy'));
                    console.log("-- lazy bg " + entry.target.classList.contains('lazy-bg'));
                    
                    let lazyImage = entry.target;

                    if (lazyImage.classList.contains('lazy')) {
                        lazyImage.src = lazyImage.dataset.src;
                        lazyImage.classList.remove("lazy");
                        lazyImageObserver.unobserve(lazyImage);

                    } else if (lazyImage.classList.contains('lazy-bg')) {    
                        lazyImage.classList.remove("lazy-bg");
                        lazyImageObserver.unobserve(lazyImage);
                    } else {
                        lazyImage.src = lazyImage.dataset.src;
                        lazyImage.classList.remove("lazy-vid");
                        lazyImageObserver.unobserve(lazyImage);
                    
                    }

                }
            });
        });

        lazyImg.forEach(function (lazyImage) {
            lazyImageObserver.observe(lazyImage);
        });
    } 
});
