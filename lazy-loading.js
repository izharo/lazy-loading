
$(document).ready(function () {

    $(window).on('resize scroll', function () {
        checIfJSIsInTheViewPort();
        checkIfCSSIsInTheViewPort();
        checkIfVIDEOIsInTheViewPort();
    });

    checIfJSIsInTheViewPort();
    checkIfCSSIsInTheViewPort();
    checkIfVIDEOIsInTheViewPort();

    //image lazy
    function checIfJSIsInTheViewPort() {
        $('.lazy').each(function (index) {
            var top_of_element = $(this).offset().top;
            var bottom_of_element = $(this).offset().top + $(this).outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).height();
            // var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
            var top_of_screen = $(window).scrollTop();
            if ((top_of_element >= top_of_screen) && (bottom_of_element <= bottom_of_screen)) {
                this.src = this.dataset.src;
                this.classList.remove("lazy");
            }
        });
    };

    //css lazy
    function checkIfCSSIsInTheViewPort() {
        $('.lazy-bg').each(function () {
            var top_of_element = $(this).offset().top;
            var bottom_of_element = $(this).offset().top + $(this).outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).height();
            // var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
            var top_of_screen = $(window).scrollTop();

            if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
                this.classList.remove("lazy-bg");
            }
        });
    };

    //video lazy
    function checkIfVIDEOIsInTheViewPort() {
        $('.lazy-vid').each(function () {
            var top_of_element = $(this).offset().top;
            var bottom_of_element = $(this).offset().top + $(this).outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
            var top_of_screen = $(window).scrollTop();
            if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
                $(this).append("<iframe src="+this.dataset.src+" allowfullscreen></iframe>");
                this.classList.remove("lazy-vid");
            }
        });
    };

});
